﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace generate_hash
{
    public class HashGenerator
    {
        private static HashidsNet.Hashids hashIds = new HashidsNet.Hashids();
        public static string Encode(long number)
        {
            return hashIds.EncodeLong(number);
        }

        public static long Decode(string hashId)
        {
            var numbers = hashIds.DecodeLong(hashId);
            long result = 0;
            for (int i = numbers.Length - 1; i >= 0; --i)
            {
                result = result * 10 + numbers[i];
            }
            return result;
        }
    }
}
